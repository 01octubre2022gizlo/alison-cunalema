package com.CunalemaCrud.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Factura {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	@Column(name = "detalle")
	private String detale;
	@Column(name = "fecha")
	private Date fecha;
	@Column(name = "valor")
	private float valor;
	
	@Column(name = "status")
	private boolean status;
	
	@Column(name="ticket_id", insertable=false, updatable=false, nullable=false)
	private long ticket_id;

	@JsonIgnore
	@ManyToOne
    @JoinColumn(name = "ticket_id",insertable=false, updatable=false,nullable = false)
	private Ticket ticket;

	public String getDetale() {
		return detale;
	}

	public void setDetale(String detale) {
		this.detale = detale;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public float getValor() {
		return valor;
	}

	public void setValor(float valor) {
		this.valor = valor;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public long getTicket_id() {
		return ticket_id;
	}

	public void setTicket_id(long ticket_id) {
		this.ticket_id = ticket_id;
	}

	public Ticket getTicket() {
		return ticket;
	}

	public void setTicket(Ticket ticket) {
		this.ticket = ticket;
	}
	
	
}
