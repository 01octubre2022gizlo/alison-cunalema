package com.CunalemaCrud.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "Tticket")
public class Ticket {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	@Column(name = "descripcion")
	private String descripcion;
	@Column(name = "tipoTrabajo")
	private String tipoTrabajo;
	@Column(name = "tiempo")
	private int tiempo;
	@Column(name = "status")
	private boolean status;
	
	@Column(name="persona_id", insertable=false, updatable=false, nullable=false)
	private long persona_id;
	
	@JsonIgnore
	@ManyToOne
    @JoinColumn(name = "persona_id", nullable = false)
	
	
	private Persona persona;

	public Ticket() {
		super();
		// TODO Auto-generated constructor stub
	}

	//constructor
	public Ticket(String descripcion, String tipoTrabajo, int tiempo, boolean status, long persona_id,
			Persona persona) {
		super();
		this.descripcion = descripcion;
		this.tipoTrabajo = tipoTrabajo;
		this.tiempo = tiempo;
		this.status = status;
		this.persona_id = persona_id;
		this.persona = persona;
	}

	//Getters and Setters
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getTipoTrabajo() {
		return tipoTrabajo;
	}

	public void setTipoTrabajo(String tipoTrabajo) {
		this.tipoTrabajo = tipoTrabajo;
	}

	public int getTiempo() {
		return tiempo;
	}
	
	
	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public void setTiempo(int tiempo) {
		this.tiempo = tiempo;
	}

	public long getPersona_id() {
		return persona_id;
	}

	public void setPersona_id(long persona_id) {
		this.persona_id = persona_id;
	}

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}
	
	
	
}
