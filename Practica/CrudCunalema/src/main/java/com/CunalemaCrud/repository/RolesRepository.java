package com.CunalemaCrud.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.CunalemaCrud.model.Roles;

public interface RolesRepository extends JpaRepository<Roles, Long> {
	List<Roles> findByNameContaining(String name);
}
