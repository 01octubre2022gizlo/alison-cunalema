package com.CunalemaCrud.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.CunalemaCrud.model.Persona;

public interface PersonaRepository  extends JpaRepository<Persona, Long>{
	List<Persona> findByNameContaining(String name);
	
	Persona findByUser(String user);

}
