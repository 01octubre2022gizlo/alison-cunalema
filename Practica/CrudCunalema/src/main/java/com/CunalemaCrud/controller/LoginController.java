package com.CunalemaCrud.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.CunalemaCrud.exception.ResourceNotFoundException;
import com.CunalemaCrud.model.Login;
import com.CunalemaCrud.model.Persona;
import com.CunalemaCrud.repository.PersonaRepository;
import com.CunalemaCrud.response.ResponseHandler;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/servicio")
public class LoginController {

	@Autowired
    private PersonaRepository employeeResposity;

    @PostMapping("/login")
    public ResponseEntity<Object> login(@RequestBody Login loginDto){
    	try {
    		Persona userEmployee = employeeResposity.findByUser(loginDto.getUser());
            if(userEmployee == null) {
                throw new ResourceNotFoundException("Usuario no Existe.");   
            }
            
            if(!userEmployee.getPassword().equals(loginDto.getPassword())){
                throw new ResourceNotFoundException("Password Incorrecto.");
            }
            return ResponseHandler.generarRespuesta("Successfully added data!", HttpStatus.OK, userEmployee);
		} catch (Exception e) {
			return ResponseHandler.generarRespuesta(e.getMessage(), HttpStatus.MULTI_STATUS, null);
		}
    	
    }
}
