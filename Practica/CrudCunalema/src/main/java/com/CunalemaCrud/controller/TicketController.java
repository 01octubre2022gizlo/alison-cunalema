package com.CunalemaCrud.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.CunalemaCrud.exception.ResourceNotFoundException;
import com.CunalemaCrud.model.Persona;
import com.CunalemaCrud.model.Ticket;
import com.CunalemaCrud.repository.PersonaRepository;
import com.CunalemaCrud.repository.TicketRepository;
import com.CunalemaCrud.response.ResponseHandler;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/servicio")
public class TicketController {
	
	@Autowired
	PersonaRepository personaRepository;
	@Autowired
	private TicketRepository ticketRepository;
	
	@GetMapping("/persona/{personaId}/obtenerTicket")
	  public ResponseEntity<List<Ticket>> getAllHoursByEmployeeId(@PathVariable(value = "employeeId") Long employeeId) {
	    if (!personaRepository.existsById(employeeId)) {
	    	throw new ResourceNotFoundException("No se encontro Empleado con id = " + employeeId);
	    }
	    List<Ticket> detailHour = ticketRepository.findByPersonaId(employeeId);
	    return new ResponseEntity<>(detailHour, HttpStatus.OK);
	  }
	
	@GetMapping("/obtenerTicket/{id}")
	  public ResponseEntity<Ticket> getTicketByPersonaId(@PathVariable(value = "id") Long id) {
	    Ticket detailHours = ticketRepository.findById(id)
      .orElseThrow(() -> new ResourceNotFoundException("No se encontro Detalle de Hora con id = " + id));
	    return new ResponseEntity<>(detailHours, HttpStatus.OK);
	  }
	
	 
	@PostMapping("/persona/{persona_id}/crearTicket")
	public ResponseEntity<Object> createTicket(@RequestBody Ticket ticket) {
		try {
			Optional<Persona> personaOpt =  personaRepository.findById(ticket.getPersona_id());
			Persona persona = personaOpt.get();
			Ticket _ticket = ticketRepository
					.save(new Ticket(ticket.getDescripcion(), 
							ticket.getTipoTrabajo(), 
							ticket.getTiempo(),
									  false,
									  persona.getId(),persona));
			
			return ResponseHandler.generarRespuesta("Successfully added data!", HttpStatus.OK,_ticket );
			
		} catch (Exception e) {
			return ResponseHandler.generarRespuesta(e.getMessage(), HttpStatus.MULTI_STATUS, null);
		}
	}
	  
	  @PutMapping("/actualizarTicket/{id}")	
	  public ResponseEntity<Ticket> updateTicket(@PathVariable("id") long id, @RequestBody Ticket ticketRequest) {
		  Ticket ticket = ticketRepository.findById(id)
	        .orElseThrow(() -> new ResourceNotFoundException("Ticket Td " + id + "not encontrado"));
	    return new ResponseEntity<>(ticketRepository.save(ticket), HttpStatus.OK);
	  }
	
}
