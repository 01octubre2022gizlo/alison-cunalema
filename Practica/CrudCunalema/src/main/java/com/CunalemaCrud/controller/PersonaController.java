package com.CunalemaCrud.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.CunalemaCrud.model.Persona;
import com.CunalemaCrud.model.Roles;
import com.CunalemaCrud.repository.PersonaRepository;
import com.CunalemaCrud.repository.RolesRepository;
import com.CunalemaCrud.response.ResponseHandler;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/servicio")
public class PersonaController {

	@Autowired
	PersonaRepository personaRepository;
	@Autowired
	RolesRepository rolesRepository;
	@GetMapping("/prueba")
	public ResponseEntity<List<Persona>> obtenerprueba(@RequestParam(required = false) String name) {
		try {
			List<Persona> personas = new ArrayList<Persona>();
			Persona persona= new Persona();
			persona.setName("alison cunalema");
			personas.add(persona);
			return new ResponseEntity<>(personas, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}	
	@GetMapping("/consultarPersona")
	public ResponseEntity<List<Persona>> getAllPersona(@RequestParam(required = false) String name) {
		try {
			List<Persona> personas = new ArrayList<Persona>();
			if (name == null)
				personaRepository.findAll().forEach(personas::add);
			else
				personaRepository.findByNameContaining(name).forEach(personas::add);
			if (personas.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
			return new ResponseEntity<>(personas, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PostMapping("/crearPersona")
	public ResponseEntity<Object> createPersona(@RequestBody Persona persona) {
		try {
			Optional<Roles> rolOpt =  rolesRepository.findById(persona.getRol_id());
			Roles rol = rolOpt.get();
			Persona _employee = personaRepository
					.save(new Persona(persona.getName(), 
									  persona.getLastname(), 
									  persona.getUser(),
									  persona.getPassword(),
									  false,
									  rol,
									  persona.getRol_id(),
									  persona.getCorreo(),
									  persona.getCedula()));
			
			return ResponseHandler.generarRespuesta("Successfully added data!", HttpStatus.OK,_employee );
			
		} catch (Exception e) {
			return ResponseHandler.generarRespuesta(e.getMessage(), HttpStatus.MULTI_STATUS, null);
		}
	}
	
	@PutMapping("/actualizarPersona/{id}")
	public ResponseEntity<Object> updatePersona(@PathVariable("id") long id, @RequestBody Persona persona) {
		Optional<Persona> personaData = personaRepository.findById(id);
		Optional<Roles> rolOpt =  rolesRepository.findById(persona.getRol_id());
		Roles rol = rolOpt.get();
		Persona _persona = personaData.get();
		try {
			if (personaData.isPresent()) {
				if (!rol.getName().contains("jefe")  ) {
					_persona.setName(persona.getName());
					_persona.setLastname(persona.getLastname());
					_persona.setRol(rol);
					_persona.setRol_id(persona.getRol_id());
					_persona.setCorreo(persona.getCorreo());
					_persona.setCedula(persona.getCedula());
					return ResponseHandler.generarRespuesta("Successfully added data!", HttpStatus.OK, _persona );
				}
				else {
					_persona.setName(persona.getName());
					_persona.setLastname(persona.getLastname());
					_persona.setRol(rol);
					_persona.setRol_id(rol.getId());
					_persona.setCorreo(persona.getCorreo());
					_persona.setCedula(persona.getCedula());					
					return ResponseHandler.generarRespuesta("Successfully added data!", HttpStatus.OK, _persona );
				}
				
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
			
		} catch (Exception e) {
			return ResponseHandler.generarRespuesta(e.getMessage(), HttpStatus.MULTI_STATUS, null);
		}
	}
	
	@DeleteMapping("/eliminarPersona/{id}")
	public ResponseEntity<Object> deletePersona(@PathVariable("id") long id) {
		try {
			personaRepository.deleteById(id);
			return ResponseHandler.generarRespuesta("Deleted!", HttpStatus.OK, null);
		} catch (Exception e) {
			return ResponseHandler.generarRespuesta(e.getMessage(), HttpStatus.MULTI_STATUS, null);
		}
		
	}
}
