import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { EmployeeI, ResponseApiEmployeeI } from '../interfaces/employee.interface';

@Injectable({
  providedIn: 'root'
})
export class ServicesEmployee {

  baseURL= 'http://localhost:8023/servicio/crearPersona';
  baseURLUSERS= 'http://localhost:8023/servicio/consultarPersona';

  baseURLUSERSACT= 'http://localhost:8023/servicio/actualizarPersona/';
  baseURLUSERSDEL= 'http://localhost:8023/servicio/eliminarPersona/';
  baseproductos= 'https://fakestoreapi.com/products';

  constructor(private http: HttpClient) { }

  postEmployee(employee: EmployeeI): Observable<ResponseApiEmployeeI>{    
    return this.http.post<ResponseApiEmployeeI>(this.baseURL, employee);
 }
 getusuarios(){
  return this.http.get<[]>(this.baseURLUSERS);
}
getproductos(){
  return this.http.get<[]>(this.baseproductos);
}
updateusuario(usuario:any){
  console.log(usuario)
  
  return this.http.put<[]>(this.baseURLUSERSACT+usuario.id,usuario)
}

deleteuser(user:any){
  console.log(user)
  
  return this.http.delete<[]>(this.baseURLUSERSDEL+user.id,user)
}
}
