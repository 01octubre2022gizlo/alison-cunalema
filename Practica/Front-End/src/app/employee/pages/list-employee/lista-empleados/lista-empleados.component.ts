import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ServicesEmployee } from '../../../services/servicesEmployee.service';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-lista-empleados',
  templateUrl: './lista-empleados.component.html',
  styleUrls: ['./lista-empleados.component.css']
})
export class ListaEmpleadosComponent implements OnInit {

  constructor(    private _formBuilder: FormBuilder,
    private _router: Router,
    private _authService: ServicesEmployee,) { }
 usu=[{
  id:0,
  name:"",
  lastname:"",
  user:"",
  correo:"",
  cedula:"",
  rol_id:"",
  password:"",
  status:""
}]

usuedit={
  id:0,
  name:"",
  lastname:"",
  user:"",
  correo:"",
  cedula:"",
  rol_id:"",
  password:"",
  status:""
}
searchText: any;
veredit=false;

  ngOnInit(): void {
    this.veredit=false;
    this._authService.getusuarios().subscribe(data=>{
      this.usu=data;
      console.log(this.usu);
    })

   
  }

  editar(id:any){
    
    console.log(id);
    for (let i=0; i < this.usu.length; i++){
      
      if (this.usu[i].id==id){
          this.usuedit=this.usu[i];
          this.veredit=true;
          console.log(this.usuedit);
      }
  
    }
    
  }


  actualizar(usuario:any){
    

    this._authService.updateusuario(usuario).subscribe(data=>{
     
      console.log(data);
      if(data){
        alert("actualizado")
        location.reload();
      }
      
    })
   
}

delete(user:any){

  this._authService.deleteuser(user).subscribe(data=>{
   
    console.log(data);
    if(data){
      alert("eliminado")
     // location.reload();
    }
    
  })
}

}
