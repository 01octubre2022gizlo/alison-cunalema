import { Component, OnInit } from '@angular/core';
import { ServicesEmployee } from '../../services/servicesEmployee.service';

@Component({
  selector: 'app-create-detail-hours',
  templateUrl: './create-detail-hours.component.html',
  styleUrls: ['./create-detail-hours.component.css']
})
export class CreateDetailHoursComponent implements OnInit {

  constructor( private _authService: ServicesEmployee) { }
productos=[{
  id:0,
  title:"",
  price:"",
  description:"",
  category:"",
  image:"",
  rating:0
}]
  ngOnInit(): void {

    this._authService.getproductos().subscribe(data=>{
      if (data){
        this.productos=data;
        console.log(data)
      }
    })
  }

}
