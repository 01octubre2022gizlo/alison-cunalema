import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateDetailHoursComponent } from './employee/pages/create-detail-hours/create-detail-hours.component';
import { CreateEmployeeComponent } from './employee/pages/create-employee/create-employee.component';
import { ListaEmpleadosComponent } from './employee/pages/list-employee/lista-empleados/lista-empleados.component';
import { LoginComponent } from './user/login/login.component';
import { BrowserModule } from '@angular/platform-browser'

const routes: Routes = [
  {
    path: '',
    component: LoginComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'registroEmpleados',
    component: CreateEmployeeComponent
  },
  {
    path: 'inicio',
    component: CreateDetailHoursComponent
  },
  {
    path: 'usuarios',
    component: ListaEmpleadosComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes),BrowserModule],
  exports: [RouterModule]
})
export class AppRoutingModule { }
